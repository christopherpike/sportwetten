/*
 * R.Schneider
 * Projekt Sportwetten
 * 23.03.2018
 * //git clone https://christopherpike@bitbucket.org/christopherpike/*.git
 * // git commit -m "Initial commit" git push -u origin master      https://docs.oracle.com/javase/6/docs/
 * Tipp
 */

//package sportwetten

import java.io.*;

class Tipp implements Serializable,Comparable
{
	private static int tipp_anzahl=0;
	
	private final int tipp_id;
	
	private final Spieler spieler;
	
	private final String tipp;
	
	private final int betrag_euro_tipp;
	
	
	//-------------
	
	public Tipp(Spieler spieler,String tipp,int betrag_euro_tipp)
	{
		this.tipp_id=++tipp_anzahl;
		
		this.spieler=spieler;
		
		this.tipp=tipp.trim();
		
		this.betrag_euro_tipp=spieler.auszahlen_Guthaben_euro(betrag_euro_tipp);
	}
	
	//---get----------
	
	public int getTipp_anzahl()
	{
		return tipp_anzahl;
	}

	public int getTipp_id()
	{
		return tipp_id;
	}
	
	public Spieler getSpieler()
	{
		return spieler;
	}
	
	public String getTipp()
	{
		return tipp;
	}
	
	public int getBetrag_euro_tipp()
	{
		return betrag_euro_tipp;
	}
	
	public String dossier()
	{
		return "Tipp "+getTipp_id()+" von "+spieler.getBezeichnung()+" mit "+getBetrag_euro_tipp()+" auf "+getTipp();
	}
	
	//---set----------
	
	
	//---overrides----------

	@Override
	public int hashCode()
	{
		//return spieler.getNach_name().hashCode();
		return tipp_id+spieler.getNach_name().hashCode();
	}
	
	//equals
	@Override
	public boolean equals(Object obj)
	{
		if(null!=obj)
		{
			if(obj instanceof Tipp)
			{
				Tipp temp=(Tipp)obj;

				if(temp.getSpieler().getSpieler_id()==getSpieler().getSpieler_id())
				{
					if(temp.getSpieler().getNach_name().equals(getSpieler().getNach_name() ) )
					{
						return true;
					}
					else
					{
						return false;
					}
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		return false;
	}
	
	@Override
	public int compareTo(Object obj)
	{
		Tipp temp=(Tipp)obj;
		
		if(getTipp_id()<temp.getTipp_id())
		{
			return -1;
		}
		else if(getTipp_id()>temp.getTipp_id())
		{
			return 1;
		}
		
		return 0;
	}
}
