/*
 * R.Schneider
 * Projekt Sportwetten
 * 06.04.2018
 * //git clone https://christopherpike@bitbucket.org/christopherpike/*.git
 * // git commit -m "Initial commit" git push -u origin master      https://docs.oracle.com/javase/6/docs/
 * View
 */

//package sportwetten

import java.util.*;
import java.awt.*;
import java.awt.event.*;
//import Werkzeug;

class View extends Frame
{
	private ActionListener cationlistener;

	private Panel pnl_spieler_anlegen;
	
	private Panel pnl_spieler_anmelden;
	
	private Panel pnl_uebersicht_spieler_anzeigen;
	
	private Panel pnl_sportereignis_anlegen;

	private Panel pnl_uebersicht_sportereignis_anzeigen;

	private Panel pnl_wette_anlegen;

	private Panel pnl_uebersicht_wette_anzeigen;
	
	private Panel pnl_tipp_abgeben;
	
	private Panel pnl_uebersicht_tipp_anzeigen;
	
	private MenuItem menuitem;

	private Menu menu;
	
	private MenuBar menubar;
	
	private Boolean bol=true;
	
	
	//---------------
	
	public View(ActionListener cactionlistener)
	{
		this.cationlistener=cactionlistener;

		setSize(800,400);
		setLocation(100,100);
		setLayout(new CardLayout());

		//----
		menubar=new MenuBar();
		
		pnl_spieler_anlegen=new Panel();

		pnl_spieler_anmelden=new Panel();

		pnl_uebersicht_spieler_anzeigen=new Panel();

		pnl_sportereignis_anlegen=new Panel();

		pnl_uebersicht_sportereignis_anzeigen=new Panel();

		pnl_wette_anlegen=new Panel();

		pnl_uebersicht_wette_anzeigen=new Panel();

		pnl_tipp_abgeben=new Panel();

		pnl_uebersicht_tipp_anzeigen=new Panel();

		fuelle_menubar(cactionlistener);
		//fuelle_menubar();

		setMenuBar(menubar);

		fuelle_panel_spieler_anlegen();
		//add(pnl_spieler_anlegen);

		fuelle_panel_spieler_anmelden();
		add(pnl_spieler_anmelden);

		fuelle_panel_uebersicht_spieler_anzeigen(5,"Jogi","L�w","fuss8all",254.5F,2);
		//add(pnl_uebersicht_spieler_anzeigen);

		fuelle_panel_sportereignis_anlegen();
		//add(pnl_sportereignis_anlegen);

		//(int sportereignis_id,String sp_e_sportart,String sp_e_mannschaft_heim,String sp_e_mannschaft_gast,
		//		String sp_e_datum,String sp_e_uhrzeit,String sp_e_status,String sp_e_endergebins,int endergebnis_heim,int endergebins_gast)
		fuelle_panel_uebersicht_sportereignis_anzeigen(125,"Tennis","Roger Federer","Steffi Graf","20.02.1998","20:00","gelaufen","Alle haben (sich lieb) gewonnen",2,2);
		//add(pnl_uebersicht_sportereignis_anzeigen);
		
		fuelle_panel_wette_anlegen();
		//add(pnl_wette_anlegen);

		//fuelle_panel_uebersicht_wette_anzeigen(int wette_id,String sportart,String sportereignis,float quote_sieg_heim,float quote_unentschieden,float quote_niederlage_heim
		//String wette_status, String wetten_ersteller)
		fuelle_panel_uebersicht_wette_anzeigen(3,"Handball","TUS L�bbecke - GWD Minden",2.5F,1F,3.0F,"offen","boris becker");
		//add(pnl_uebersicht_wette_anzeigen);

		fuelle_panel_tipp_abgeben();
		//add(pnl_tipp_abgeben);
		
		//(int tipp_id,String sportart,String sportereignis,String tipp_art,int tipp_betrag_euro,String wette_status,String wette_ablauf_datum)//
		fuelle_panel_uebersicht_tipp_anzeigen(5,"Basketball","Boston Maverics - New York Nicks","Sieg Heim-Mannschaft",34,"offen","22.02.2019 um 18:00 Uhr");
		//add(pnl_uebersicht_tipp_anzeigen);

		//-----
		addWindowListener(new RActionListener());
		//addActionListener(new RActionListener());
		//addActionListener(new RActionListener());
		setVisible(true);
	}
	
	//-------------
	
	public void fuelle_menubar(ActionListener cactionlistener)
	{
			Menu menu_datei=new Menu("Datei");
			
			Menu menu_spieler=new Menu("Spieler");

			Menu menu_sp_e=new Menu("Sportereignis");

			Menu menu_wette=new Menu("Wette");

			Menu menu_tipp=new Menu("Tipp");
			
			Menu menu_bol=new Menu(bol.toString());

			Menu menu_help=new Menu("?");

			//----------------

			MenuItem m_datei_x=new MenuItem();
			
			MenuItem m_spieler_anlegen=new MenuItem("Spieler anlegen");
			m_spieler_anlegen.addActionListener(cactionlistener);

			MenuItem m_spieler_anmelden=new MenuItem("Spieler anmelden");
			m_spieler_anmelden.addActionListener(cactionlistener);
			
			MenuItem m_spieler_zusammenfassung=new MenuItem("Spieler Zusammenfassung anzeigen");
			m_spieler_zusammenfassung.addActionListener(cactionlistener);
			
			MenuItem m_spieler_betrag_einzahlen=new MenuItem("Spieler Betrag einzahlen");
			m_spieler_betrag_einzahlen.addActionListener(cactionlistener);
			
			MenuItem m_spieler_betrag_auszahlen=new MenuItem("Spieler betrag auszahlen");
			m_spieler_betrag_auszahlen.addActionListener(cactionlistener);
			
			//-----------
			
			MenuItem m_sp_e_anlegen=new MenuItem("Sportereignis anlegen");
			m_sp_e_anlegen.addActionListener(cactionlistener);
			
			MenuItem m_sp_e_anzeigen=new MenuItem("Sportereignis anzeigen");
			m_sp_e_anzeigen.addActionListener(cactionlistener);
			
			MenuItem m_sp_e_anzeigen_nur_ohne_endergebnis=new MenuItem("Nur Sportereignisse ohne Endergebnis anzeigen");
			m_sp_e_anzeigen_nur_ohne_endergebnis.addActionListener(cactionlistener);
			
			MenuItem m_sp_e_anzeigen_nur_mit_endergebnis=new MenuItem("Nur Sportereignisse mit Endergebnis anzeigen");
			m_sp_e_anzeigen_nur_mit_endergebnis.addActionListener(cactionlistener);
			
			MenuItem m_sp_e_endergebnis_eingeben=new MenuItem("Sportereignis Endergebnis eingeben");
			m_sp_e_endergebnis_eingeben.addActionListener(cactionlistener);
			
			//-------
			
			MenuItem m_wette_anlegen=new MenuItem("Wette anlegen");
			m_wette_anlegen.addActionListener(cactionlistener);
			
			MenuItem m_wette_anzeigen=new MenuItem("Wette anzeigen");
			m_wette_anzeigen.addActionListener(cactionlistener);
			
			MenuItem m_wette_anzeigen_nur_offene=new MenuItem("Nur offene Wetten anzeigen");
			m_wette_anzeigen_nur_offene.addActionListener(cactionlistener);
			
			MenuItem m_wette_anzeigen_nur_geschlossene=new MenuItem("Nur geschlossene Wetten anzeigen");
			m_wette_anzeigen_nur_geschlossene.addActionListener(cactionlistener);
			
			MenuItem m_wette_stornieren=new MenuItem("Wette stronieren");
			m_wette_stornieren.addActionListener(cactionlistener);
			
			//------
			
			MenuItem m_tipp_anlegen=new MenuItem("Tipp anlegen");
			m_tipp_anlegen.addActionListener(cactionlistener);

			MenuItem m_tipp_anzeigen_nur_eigne=new MenuItem("Eigene Tipps anzeigen");
			m_tipp_anzeigen_nur_eigne.addActionListener(cactionlistener);
			
			MenuItem m_tipp_anzeigen_nur_eigne_offene=new MenuItem("Offene eigene Tipps anzeigen");
			m_tipp_anzeigen_nur_eigne_offene.addActionListener(cactionlistener);

			MenuItem m_tipp_anzeigen_nur_eigne_geschlossene=new MenuItem("Geschlossene eigene Tipps anzeigen");
			m_tipp_anzeigen_nur_eigne_geschlossene.addActionListener(cactionlistener);

			MenuItem m_tipp_anzeigen_nur_eigne_geschlossene_verloren=new MenuItem("Geschlossene, verlorene eigene Tipps anzeigen");
			m_tipp_anzeigen_nur_eigne_geschlossene_verloren.addActionListener(cactionlistener);

			MenuItem m_tipp_anzeigen_nur_eigne_geschlossen_gewonnen=new MenuItem("Geschlossene, gewonnene eigene Tipps anzeigen");
			m_tipp_anzeigen_nur_eigne_geschlossen_gewonnen.addActionListener(cactionlistener);


			MenuItem m_tipp_anzeigen_alle=new MenuItem("Tipps anzeigen");
			m_tipp_anzeigen_alle.addActionListener(cactionlistener);

			MenuItem m_tipp_anzeigen_nur_offene=new MenuItem("Offene Tipps anzeigen");
			m_tipp_anzeigen_nur_offene.addActionListener(cactionlistener);

			MenuItem m_tipp_anzeigen_nur_geschlossene=new MenuItem("Geschlossene Tipps anzeigen");
			m_tipp_anzeigen_nur_geschlossene.addActionListener(cactionlistener);

			MenuItem m_tipp_anzeigen_nur_geschlossene_verloren=new MenuItem("Geschlossene, verlorene Tipps anzeigen");
			m_tipp_anzeigen_nur_geschlossene_verloren.addActionListener(cactionlistener);

			MenuItem m_tipp_anzeigen_nur_geschlossen_gewonnen=new MenuItem("Geschlossene, gewonnene Tipps anzeigen");
			m_tipp_anzeigen_nur_geschlossen_gewonnen.addActionListener(cactionlistener);
			
			//---------


			MenuItem m_help=new MenuItem("H�lfe, H�lfe");

			//---------

			menu_spieler.add(m_spieler_anlegen);
			
			
			menu_spieler.add(m_spieler_anmelden);
			menu_spieler.add(m_spieler_zusammenfassung);
			menu_spieler.add(m_spieler_betrag_einzahlen);
			menu_spieler.add(m_spieler_betrag_auszahlen);

			//-----------

			menu_sp_e.add(m_sp_e_anlegen);
			menu_sp_e.add(m_sp_e_anzeigen);
			menu_sp_e.add(m_sp_e_anzeigen_nur_ohne_endergebnis);
			menu_sp_e.add(m_sp_e_anzeigen_nur_mit_endergebnis);
			menu_sp_e.add(m_sp_e_endergebnis_eingeben);
			
			//------------------------
			
			menu_wette.add(m_wette_anlegen);
			menu_wette.add(m_wette_anzeigen);
			menu_wette.add(m_wette_anzeigen_nur_offene);
			menu_wette.add(m_wette_anzeigen_nur_geschlossene);
			menu_wette.add(m_wette_stornieren);
			
			//---------------

			menu_tipp.add(m_tipp_anlegen);
			menu_tipp.add(m_tipp_anzeigen_nur_eigne);
			menu_tipp.add(m_tipp_anzeigen_nur_eigne_offene);
			menu_tipp.add(m_tipp_anzeigen_nur_eigne_geschlossene);
			menu_tipp.add(m_tipp_anzeigen_nur_eigne_geschlossene_verloren);
			menu_tipp.add(m_tipp_anzeigen_nur_eigne_geschlossen_gewonnen);

			menu_tipp.add(m_tipp_anzeigen_alle);
			menu_tipp.add(m_tipp_anzeigen_nur_offene);
			menu_tipp.add(m_tipp_anzeigen_nur_geschlossene);
			menu_tipp.add(m_tipp_anzeigen_nur_geschlossene_verloren);
			menu_tipp.add(m_tipp_anzeigen_nur_geschlossen_gewonnen);

   			//------------
   			
   			menu_help.add(m_help);
   			
   			//-------------

			menubar.add(menu_datei);
			
			menubar.add(menu_spieler);
			
			menubar.add(menu_sp_e);
			
			menubar.add(menu_wette);
			
			menubar.add(menu_tipp);
			
			menubar.setHelpMenu(menu_help);
			
			menubar.add(menu_bol);
	}



	public void fuelle_panel_spieler_anlegen()
	{
		Label lbl_sp_an_ueberschrift=new Label("Hier k�nnen Sie einen Spieler mit Vornamen, Nachnamen und Passwort anlegen, wenn er noch nicht exisistiert.");
		
  		Label lbl_sp_an_vor_name=new Label("Spieler Vorname:");

		TextField txt_sp_an_vor_name=new TextField("Maximilian");

		Label lbl_sp_an_nach_name=new Label("Spieler Nachname:");

		TextField txt_sp_an_nach_name=new TextField("Mustermann");

		Label lbl_sp_an_passwort=new Label("Spieler Passwort:");

		TextField txt_sp_an_passwort=new TextField("geh6im");

		Button bnt_spieler_anlegen=new Button("Spieler anlegen");
		bnt_spieler_anlegen.addActionListener(cationlistener);

		//------

		pnl_spieler_anlegen.setLayout(new GridLayout(10,0));

		//----------

		pnl_spieler_anlegen.add(lbl_sp_an_ueberschrift);
		
		pnl_spieler_anlegen.add(lbl_sp_an_vor_name);
		
		pnl_spieler_anlegen.add(txt_sp_an_vor_name);

		pnl_spieler_anlegen.add(lbl_sp_an_nach_name);
		
		pnl_spieler_anlegen.add(txt_sp_an_nach_name);

		pnl_spieler_anlegen.add(lbl_sp_an_passwort);
		
		pnl_spieler_anlegen.add(txt_sp_an_passwort);
		
		pnl_spieler_anlegen.add(bnt_spieler_anlegen);
	}

	public void fuelle_panel_spieler_anmelden()
	{
		Label lbl_sp_an_ueberschrift=new Label("Hier k�nnen Sie einen Spieler mit Vornamen, Nachnamen und Passwort anmelden.");

  		Label lbl_sp_an_vor_name=new Label("Spieler Vorname:");

		TextField txt_sp_an_vor_name=new TextField("Maximilian");

		Label lbl_sp_an_nach_name=new Label("Spieler Nachname:");

		TextField txt_sp_an_nach_name=new TextField("Mustermann");

		Label lbl_sp_an_passwort=new Label("Spieler Passwort:");

		TextField txt_sp_an_passwort=new TextField("geh6im");

		Button bnt_spieler_anmelden=new Button("Spieler anmelden");
		bnt_spieler_anmelden.addActionListener(cationlistener);
		//----

		pnl_spieler_anmelden.setLayout(new GridLayout(10,0));

		//------------

		pnl_spieler_anmelden.add(lbl_sp_an_ueberschrift);

		pnl_spieler_anmelden.add(lbl_sp_an_vor_name);

		pnl_spieler_anmelden.add(txt_sp_an_vor_name);

		pnl_spieler_anmelden.add(lbl_sp_an_nach_name);

		pnl_spieler_anmelden.add(txt_sp_an_nach_name);

		pnl_spieler_anmelden.add(lbl_sp_an_passwort);

		pnl_spieler_anmelden.add(txt_sp_an_passwort);

		pnl_spieler_anmelden.add(bnt_spieler_anmelden);
	}

	public void fuelle_panel_uebersicht_spieler_anzeigen(int spieler_id,String spieler_vor_name,String spieler_nach_name,String spieler_passwort,
				float spieler_guthaben_euro,int spieler_offene_wetten)
	{
		//Anzeige mit:

		Label lbl_sp_uebers_ueberschrift=new Label("Spieler �bersicht mit ID, Vornamen, Nachnamen, Passwort, Guthaben und offenen Wetten.");

		Label lbl_sp_uebers_id=new Label("Spieler ID : "+spieler_id);

		Label lbl_sp_uebers_vor_name=new Label("Spieler-Vorname : "+spieler_vor_name);
		
		Label lbl_sp_uebers_nach_name=new Label("Spieler-Nachname : "+spieler_nach_name);
		
		Label lbl_sp_uebers_passwort=new Label("Spieler-Passwort : "+spieler_passwort);
		
		Label lbl_sp_uebers_guthaben_euro=new Label("Guthaben Euro : "+spieler_guthaben_euro);

		Label lbl_sp_uebers_offene_wetten=new Label("Offene Wetten : "+spieler_offene_wetten);
		
		//--------------
		
		pnl_uebersicht_spieler_anzeigen.setLayout(new GridLayout(10,0));
		
		//-----------
		
		pnl_uebersicht_spieler_anzeigen.add(lbl_sp_uebers_ueberschrift);
		
		pnl_uebersicht_spieler_anzeigen.add(lbl_sp_uebers_id);
		
		pnl_uebersicht_spieler_anzeigen.add(lbl_sp_uebers_vor_name);
		
		pnl_uebersicht_spieler_anzeigen.add(lbl_sp_uebers_nach_name);
		
		pnl_uebersicht_spieler_anzeigen.add(lbl_sp_uebers_passwort);
		
		pnl_uebersicht_spieler_anzeigen.add(lbl_sp_uebers_guthaben_euro);
		
		pnl_uebersicht_spieler_anzeigen.add(lbl_sp_uebers_offene_wetten);
	}

	public void fuelle_panel_sportereignis_anlegen()
	{
		//Sportereignis anlegen: 1 von 4 Sportarten, Mannschaften, Spieler eingeben, Spiel-Datum und Spiel-Startzeitpunkt angeben

		Label lbl_sp_e_an_ueberschrift=new Label("Hier k�nnen Sie ein Sportereignis anlegen. Sie m�ssen eine von vier Sportarten ausw�hlen. \nDann k�nnen Sie eine "+
		"Heim-Mannschaft und eine Gast-Mannschaft eingeben.");

		Label lbl_sp_e_an_ueberschrift_2=new Label("Danach m�ssen Sie ein Spiel-Datum und eine Spielstart-Uhrzeit eingeben. Das Sportereignis ist unver�nderlich, es kann nur das "+
		"Ergebins nach \"Ablauf der Spielzeit\" eingegeben werden.");

		Label lbl_sp_e_an_ueberschrift_3=new Label("Ein Sportereignis ist die Voraussetztung f�r eine Wette, welche die Vorausetztung f�r einen Tipp ist.");


		Label lbl_sp_e_an_sportart=new Label("Sportereignis-Sportart: Fussball oder Tennis oder Handball oder Basketball");

		TextField txt_sp_e_an_sportart=new TextField("Sportart");

		Label lbl_sp_e_an_mannschaft_heim=new Label("Heim-Mannschaft (Team oder Spieler) : ");

		TextField txt_sp_e_an_mannschaft_heim=new TextField("Heim-Mannschaft");

		Label lbl_sp_e_an_mannschaft_gast=new Label("Gast-Mannschaft (Team oder Spieler) : ");

		TextField txt_sp_e_an_mannschaft_gast=new TextField("Gast-Mannschaft");


		Label lbl_sp_e_an_sp_e_datum=new Label("Sportereignis-Datum : ");

		TextField txt_sp_e_an_sp_e_datum=new TextField(new Date().toString()); // nur Datum

		Label lbl_sp_e_an_sp_e_uhrzeit=new Label("Spielstart-Uhrzeit : ");

		TextField txt_sp_e_an_sp_e_uhrzeit=new TextField(new Date().toString()); // nur Uhrzeit


		Button bnt_sportereignis_anlegen=new Button("Sportereignis anlegen");
		bnt_sportereignis_anlegen.addActionListener(cationlistener);
		
		//--------------

		pnl_sportereignis_anlegen.setLayout(new GridLayout(16,0));

		//-----------


		pnl_sportereignis_anlegen.add(lbl_sp_e_an_ueberschrift);
		
		pnl_sportereignis_anlegen.add(lbl_sp_e_an_ueberschrift_2);
		
		pnl_sportereignis_anlegen.add(lbl_sp_e_an_ueberschrift_3);
		
		pnl_sportereignis_anlegen.add(lbl_sp_e_an_sportart);

		pnl_sportereignis_anlegen.add(txt_sp_e_an_sportart);

		pnl_sportereignis_anlegen.add(lbl_sp_e_an_mannschaft_heim);
		
		pnl_sportereignis_anlegen.add(txt_sp_e_an_mannschaft_heim);

		pnl_sportereignis_anlegen.add(lbl_sp_e_an_mannschaft_gast);

		pnl_sportereignis_anlegen.add(txt_sp_e_an_mannschaft_gast);
		
		
		pnl_sportereignis_anlegen.add(lbl_sp_e_an_sp_e_datum);

		pnl_sportereignis_anlegen.add(txt_sp_e_an_sp_e_datum);

		pnl_sportereignis_anlegen.add(lbl_sp_e_an_sp_e_uhrzeit);

		pnl_sportereignis_anlegen.add(txt_sp_e_an_sp_e_uhrzeit);
		
		
		pnl_sportereignis_anlegen.add(bnt_sportereignis_anlegen);
	}

	public void fuelle_panel_uebersicht_sportereignis_anzeigen(int sportereignis_id,String sp_e_sportart,String sp_e_mannschaft_heim,String sp_e_mannschaft_gast,
				String sp_e_datum,String sp_e_uhrzeit,String sp_e_status,String sp_e_endergebins,int sp_e_mannschaft_heim_ergebnis,int sp_e_mannschaft_gast_ergebnis)
	{


		Label lbl_sp_e_an_ueberschrift=new Label("Sportereignis �bersicht.  Wenn das Spiel noch nicht begonnen hat, oder nicht beendet ist, stehen die Ergebnisse auf Null.");
		
		Label lbl_sp_e_an_sp_e_id=new Label("Sportereignis-ID : "+sportereignis_id);///

		Label lbl_sp_e_an_sportart=new Label("Sportereignis-Sportart : "+sp_e_sportart);///

		Label lbl_sp_e_an_mannschaft_heim=new Label("Heim-Mannschaft : "+sp_e_mannschaft_heim);

		Label lbl_sp_e_an_mannschaft_gast=new Label("Gast-Mannschaft : "+sp_e_mannschaft_gast);

		Label lbl_sp_e_an_sp_e_datum=new Label("Sportereignis-Datum : "+sp_e_datum);

		Label lbl_sp_e_an_sp_e_uhrzeit=new Label("Spielstart-Uhrzeit : "+sp_e_uhrzeit);
		
		Label lbl_sp_e_an_sp_e_status=new Label("Spielstart-Uhrzeit : "+sp_e_status);
		
		Label lbl_sp_e_an_sp_e_endergebins=new Label("Endergebnis : "+sp_e_endergebins);

		Label lbl_sp_e_an_sp_e_mannschaft_heim_ergebnis=new Label("Spielstand Heim : "+sp_e_mannschaft_heim_ergebnis);
		
		Label lbl_sp_e_an_sp_e_mannschaft_gast_ergebnis=new Label("Spielstand Gast : "+sp_e_mannschaft_gast_ergebnis);

		
		Button bnt_uebersicht_sportereignis_anzeigen=new Button("Hauptmenue");
		bnt_uebersicht_sportereignis_anzeigen.addActionListener(cationlistener);
		
		
		//--------------

		pnl_uebersicht_sportereignis_anzeigen.setLayout(new GridLayout(16,0));

		//-----------


		pnl_uebersicht_sportereignis_anzeigen.add(lbl_sp_e_an_ueberschrift);

		pnl_uebersicht_sportereignis_anzeigen.add(lbl_sp_e_an_sportart);

		pnl_uebersicht_sportereignis_anzeigen.add(lbl_sp_e_an_mannschaft_heim);

		pnl_uebersicht_sportereignis_anzeigen.add(lbl_sp_e_an_mannschaft_gast);

		pnl_uebersicht_sportereignis_anzeigen.add(lbl_sp_e_an_sp_e_datum);

		pnl_uebersicht_sportereignis_anzeigen.add(lbl_sp_e_an_sp_e_uhrzeit);
		
		pnl_uebersicht_sportereignis_anzeigen.add(lbl_sp_e_an_sp_e_status);

		pnl_uebersicht_sportereignis_anzeigen.add(lbl_sp_e_an_sp_e_endergebins);
		
		pnl_uebersicht_sportereignis_anzeigen.add(lbl_sp_e_an_sp_e_mannschaft_heim_ergebnis);
		
		pnl_uebersicht_sportereignis_anzeigen.add(lbl_sp_e_an_sp_e_mannschaft_gast_ergebnis);
		
		//bnt_uebersicht_sportereignis_anzeigen

	}

	public void fuelle_panel_wette_anlegen()
	{
		//Wette anlegen: 1 von 4 Sportarten, unbewettete, ungestartete Sportereignisse anzeigen und ausw�hlen, Quoten eintragen
		
		Label lbl_wette_an_ueberschrift=new Label("Hier k�nnen Sie eine Wette anlegen. Sie m�ssen eine von vier Sportarten ausw�hlen. \nDann k�nnen Sie ein "+
		"unbewettetes Sportereignis aussuchen, von dem Sie eine Wette erstellen.");

		Label lbl_wette_an_ueberschrift_2=new Label("Danach m�ssen Sie die Quoten f�r Sieg der Heim-Manschaft, f�r ein Unentschieden und Niederlage der Heim-Mannschaft festlegen.");

		Label lbl_wette_an_ueberschrift_3=new Label("Die Wette ist unver�nderlich, sie kann nur f�r alle Tipper storniert werden. Die Tipps k�nnen nur bis zum Spielbeginn abgegeben werden. ");


		Label lbl_wette_an_sportart=new Label("Sportereignis-Sportart : Fussball oder Tennis oder Handball oder Basketball");

		TextField txt_wette_an_sportart=new TextField("Sportart");

		Label lbl_wette_an_sportereignis=new Label("Sportereignisauswahl : HIER KOMMT EINE Liste");

		TextField txt_wette_an_sportereignis=new TextField("Sportereignis");
		

		Label lbl_wette_an_quote_sieg_heim=new Label("Quote f�r Heim-Mannschaft Sieg: ");

		TextField txt_wette_an_quote_sieg_heim=new TextField("0.0");
		
		Label lbl_wette_an_quote_unentschieden=new Label("Quote f�r Unentschieden : ");

		TextField txt_wette_an_quote_unentschieden=new TextField("0.0");
		
		Label lbl_wette_an_quote_niederlage_heim=new Label("Quote f�r Heim-Mannschaft Niederlage : ");

		TextField txt_wette_an_quote_niederlage_heim=new TextField("0.0");
		

		Label lbl_wette_an_quote_status=new Label("Wetten-Status : ");

		TextField txt_wette_an_quote_status=new TextField("offen");
		

		
		Button bnt_wette_anlegen=new Button("Wette anlegen");
		bnt_wette_anlegen.addActionListener(cationlistener);
		//------
		pnl_wette_anlegen.setLayout(new GridLayout(16,0));
		//---------

		pnl_wette_anlegen.add(lbl_wette_an_ueberschrift);
		
		pnl_wette_anlegen.add(lbl_wette_an_ueberschrift_2);
		
		pnl_wette_anlegen.add(lbl_wette_an_ueberschrift_3);


		pnl_wette_anlegen.add(lbl_wette_an_sportart);
		
		pnl_wette_anlegen.add(txt_wette_an_sportart);

		pnl_wette_anlegen.add(lbl_wette_an_sportereignis);
		
		pnl_wette_anlegen.add(txt_wette_an_sportereignis);

		pnl_wette_anlegen.add(lbl_wette_an_quote_sieg_heim);
		
		pnl_wette_anlegen.add(txt_wette_an_quote_sieg_heim);

		pnl_wette_anlegen.add(lbl_wette_an_quote_unentschieden);
		
		pnl_wette_anlegen.add(txt_wette_an_quote_unentschieden);

		pnl_wette_anlegen.add(lbl_wette_an_quote_niederlage_heim);

		pnl_wette_anlegen.add(txt_wette_an_quote_niederlage_heim);

		pnl_wette_anlegen.add(lbl_wette_an_quote_status);

		pnl_wette_anlegen.add(txt_wette_an_quote_status);



		pnl_wette_anlegen.add(bnt_wette_anlegen);
		
		//System.out.println(erster_buchstabe_gross("hello"));
	}

	public void fuelle_panel_uebersicht_wette_anzeigen(int wette_id,String sportart,String sportereignis,float quote_sieg_heim,float quote_unentschieden,float quote_niederlage_heim,
				String wette_status, String wetten_ersteller)
	{
		//_uebersicht_wette_anzeigen

		//Wette anlegen: 1 von 4 Sportarten, unbewettete, ungestartete Sportereignisse anzeigen und ausw�hlen, Quoten eintragen

		Label lbl_uebersicht_wette_an_ueberschrift=new Label("Wetten-�bersicht."+"");

		//Label lbl_uebersicht_wette_an_ueberschrift_2=new Label("Danach m�ssen Sie die Quoten f�r Sieg der Heim-Manschaft, f�r ein Unentschieden und Niederlage der Heim-Mannschaft festlegen.");

		//Label lbl_uebersicht_wette_an_ueberschrift_3=new Label("Die Wette ist unver�nderlich, sie kann nur f�r alle Tipper storniert werden. Die Tipps k�nnen nur bis zum Spielbeginn abgegeben werden. ");


		Label lbl_uebersicht_wette_an_wette_id=new Label("Wetten-ID : "+wette_id);

		Label lbl_uebersicht_wette_an_sportart=new Label("Sportereignis-Sportart : "+sportart);

		Label lbl_uebersicht_wette_an_sportereignis=new Label("Sportereignisauswahl : "+sportereignis);

		Label lbl_uebersicht_wette_an_quote_sieg_heim=new Label("Quote f�r Heim-Mannschaft Sieg : "+quote_sieg_heim);

		Label lbl_uebersicht_wette_an_quote_unentschieden=new Label("Quote f�r Unentschieden : "+quote_unentschieden);

		Label lbl_uebersicht_wette_an_quote_niederlage_heim=new Label("Quote f�r Heim-Mannschaft Niederlage : "+quote_niederlage_heim);

		Label lbl_uebersicht_wette_an_quote_status=new Label("Wetten-Status : "+wette_status);
		
		Label lbl_uebersicht_wette_an_wetten_ersteller=new Label("Wetten-Ersteller : "+Werkzeug.erster_buchstabe_gross(wetten_ersteller));

		//------
		pnl_uebersicht_wette_anzeigen.setLayout(new GridLayout(16,0));
		//---------

		pnl_uebersicht_wette_anzeigen.add(lbl_uebersicht_wette_an_ueberschrift);

		//pnl_uebersicht_wette_anzeigen.add(lbl_uebersicht_wette_an_ueberschrift_2);

		//pnl_uebersicht_wette_anzeigen.add(lbl_uebersicht_wette_an_ueberschrift_3);


		pnl_uebersicht_wette_anzeigen.add(lbl_uebersicht_wette_an_wette_id);

		pnl_uebersicht_wette_anzeigen.add(lbl_uebersicht_wette_an_sportart);

		pnl_uebersicht_wette_anzeigen.add(lbl_uebersicht_wette_an_sportereignis);

		pnl_uebersicht_wette_anzeigen.add(lbl_uebersicht_wette_an_quote_sieg_heim);

		pnl_uebersicht_wette_anzeigen.add(lbl_uebersicht_wette_an_quote_unentschieden);

		pnl_uebersicht_wette_anzeigen.add(lbl_uebersicht_wette_an_quote_niederlage_heim);

		pnl_uebersicht_wette_anzeigen.add(lbl_uebersicht_wette_an_quote_status);

		pnl_uebersicht_wette_anzeigen.add(lbl_uebersicht_wette_an_wetten_ersteller);

		//pnl_uebersicht_wette_anzeigen.add(lbl_wette_an_quote_niederlage_heim);

		//pnl_uebersicht_wette_anzeigen.add(txt_wette_an_quote_niederlage_heim);

		//pnl_uebersicht_wette_anzeigen.add(lbl_wette_an_quote_status);

		//pnl_uebersicht_wette_anzeigen.add(txt_wette_an_quote_status);
		
		
		//pnl_uebersicht_wette_anzeigen.add();
	}

	public void fuelle_panel_tipp_abgeben()
	{
		//Tipp abgeben: 1 von 4 Sportarten, tippbare Sportereignisse anzeigen und ausw�hlen, Tippart w�hlen und Euro setzen

		Label lbl_tipp_abg_ueberschrift=new Label("Hier k�nnen Sie einen Tipp abgeben. Sie m�ssen eine von vier Sportarten ausw�hlen.  ");

		Label lbl_tipp_abg_ueberschrift_2=new Label("Dann k�nnen Sie ein betippbares Sportereignis aussuchen, auf dem Sie einen Tipp abgeben k�nnen.");

		Label lbl_tipp_abg_ueberschrift_3=new Label("Der Tipp ist unver�nderlich, ausser die Wette wird f�r alle Tipper storniert. Die Tipps k�nnen nur bis zum Spielbeginn abgegeben werden. ");

		Label lbl_tipp_abg_Spieler_info=new Label("Spieler : "+"Spieler"+" Guthaben Euro: "+"xxx");

		Label lbl_tipp_abg_sportart=new Label("Sportereignis-Sportart : Fussball oder Tennis oder Handball oder Basketball");

		TextField txt_tipp_abg_sportart=new TextField("Sportart");

		Label lbl_tipp_abg_sportereignis=new Label("Sportereignisauswahl : HIER KOMMT EINE Liste");

		TextField txt_tipp_abg_sportereignis=new TextField("Sportereignis");


		Label lbl_tipp_abg_tipp_art=new Label("Tipp-Art : Sieg Heim oder Unentschieden oder Niederlage Heim");

		TextField txt_tipp_abg_tipp_art=new TextField("Unentschiden");

		Label lbl_tipp_abg_tipp_betrag_euro=new Label("Tipp Betrag in Euro : ");

		TextField txt_tipp_abg_tipp_betrag_euro=new TextField("0.0");


		Button btn_tipp_abgeben=new Button("Tipp abgeben");
		
		//------
		pnl_tipp_abgeben.setLayout(new GridLayout(16,0));
		//---------


		pnl_tipp_abgeben.add(lbl_tipp_abg_ueberschrift);
		
		pnl_tipp_abgeben.add(lbl_tipp_abg_ueberschrift_2);
		
		pnl_tipp_abgeben.add(lbl_tipp_abg_ueberschrift_3);
		
		pnl_tipp_abgeben.add(lbl_tipp_abg_Spieler_info);

		pnl_tipp_abgeben.add(lbl_tipp_abg_sportart);
		
		pnl_tipp_abgeben.add(txt_tipp_abg_sportart);
		
		pnl_tipp_abgeben.add(lbl_tipp_abg_sportereignis);

		pnl_tipp_abgeben.add(txt_tipp_abg_sportereignis);
		
		pnl_tipp_abgeben.add(lbl_tipp_abg_tipp_art);

		pnl_tipp_abgeben.add(txt_tipp_abg_tipp_art);

		pnl_tipp_abgeben.add(lbl_tipp_abg_tipp_betrag_euro);

		pnl_tipp_abgeben.add(txt_tipp_abg_tipp_betrag_euro);
		
		
		pnl_tipp_abgeben.add(btn_tipp_abgeben);
		
	}

	public void fuelle_panel_uebersicht_tipp_anzeigen(int tipp_id,String sportart,String sportereignis,String tipp_art,int tipp_betrag_euro,String wette_status,String wette_ablauf_datum)//
	{
		//Tipp abgeben: 1 von 4 Sportarten, tippbare Sportereignisse anzeigen und ausw�hlen, Tippart w�hlen und Euro setzen

		Label lbl_uebersicht_tipp_an_ueberschrift=new Label("Hier k�nnen Sie einen Tipp abgeben. Sie m�ssen eine von vier Sportarten ausw�hlen.  ");

		Label lbl_uebersicht_tipp_an_ueberschrift_2=new Label("Dann k�nnen Sie ein betippbares Sportereignis aussuchen, auf dem Sie einen Tipp abgeben k�nnen.");

		Label lbl_uebersicht_tipp_an_ueberschrift_3=new Label("Der Tipp ist unver�nderlich, ausser die Wette wird f�r alle Tipper storniert. Die Tipps k�nnen nur bis zum Spielbeginn abgegeben werden. ");

		Label lbl_uebersicht_tipp_an_Spieler_info=new Label("Spieler : "+"Spieler"+" Guthaben Euro: "+"xxx");

		Label lbl_uebersicht_tipp_an_tipp_id=new Label("Tipp-ID : "+tipp_id);

		Label lbl_uebersicht_tipp_an_sportart=new Label("Sportereignis-Sportart : "+sportart);

		Label lbl_uebersicht_tipp_an_sportereignis=new Label("Sportereignisauswahl : "+sportereignis);

		Label lbl_uebersicht_tipp_an_tipp_art=new Label("Tipp-Art : "+tipp_art);

		Label lbl_uebersicht_tipp_an_tipp_betrag_euro=new Label("Tipp Betrag in Euro : "+tipp_betrag_euro);

		Label lbl_uebersicht_tipp_an_wette_status=new Label("Wetten-Status : "+wette_status);
		
		Label lbl_uebersicht_tipp_an_ablauf_datum=new Label("Wette wird am x um y ausgespielt : "+wette_ablauf_datum);

		//Label lbl_uebersicht_tipp_an_tipp_art=new Label("Tipp-Art : "+tipp_art);

		//Label lbl_uebersicht_tipp_an_tipp_betrag_euro=new Label("Tipp Betrag in Euro : "+tipp_betrag_euro);


		//------
		pnl_uebersicht_tipp_anzeigen.setLayout(new GridLayout(16,0));
		//---------


		pnl_uebersicht_tipp_anzeigen.add(lbl_uebersicht_tipp_an_ueberschrift);

		//pnl_tipp_abgeben.add(lbl_uebersicht_tipp_an_ueberschrift_2);

		//pnl_tipp_abgeben.add(lbl_uebersicht_tipp_an_ueberschrift_3);

		pnl_uebersicht_tipp_anzeigen.add(lbl_uebersicht_tipp_an_Spieler_info);

		pnl_uebersicht_tipp_anzeigen.add(lbl_uebersicht_tipp_an_tipp_id);

		pnl_uebersicht_tipp_anzeigen.add(lbl_uebersicht_tipp_an_sportart);

		pnl_uebersicht_tipp_anzeigen.add(lbl_uebersicht_tipp_an_sportereignis);

		pnl_uebersicht_tipp_anzeigen.add(lbl_uebersicht_tipp_an_tipp_art);

		pnl_uebersicht_tipp_anzeigen.add(lbl_uebersicht_tipp_an_tipp_betrag_euro);

		pnl_uebersicht_tipp_anzeigen.add(lbl_uebersicht_tipp_an_wette_status);

		pnl_uebersicht_tipp_anzeigen.add(lbl_uebersicht_tipp_an_ablauf_datum);

		//pnl_uebersicht_tipp_anzeigen.add(txt_tipp_abg_tipp_betrag_euro);




		//pnl_uebersicht_tipp_anzeigen.add(lbl_tipp_abg_ueberschrift);
	}
	
	public void layouts()
	{
		Panel clp=new Panel();
		clp.setLayout(new CardLayout(100,100));
		
		
	}
	
	
	//http://bashrcgenerator.com/
	//http://www.bash.academy/
	//http://www.tldp.org/LDP/Bash-Beginners-Guide/html/
	//http://tldp.org/HOWTO/Bash-Prog-Intro-HOWTO.html
	//https://regexr.com/
	
	
	

	//------get----------------
	
	
	
	
	
	//-------set---------------



	//----------------

	class RActionListener extends WindowAdapter
	{

		@Override
		public void windowClosing(WindowEvent we)
		{
			dispose();
			setVisible(false);
			System.exit(0);
		}
	}

}
