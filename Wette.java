/*
 * R.Schneider
 * Projekt Sportwetten
 * 29.03.2018
 * //git clone https://christopherpike@bitbucket.org/christopherpike/*.git
 * // git commit -m "Initial commit" git push -u origin master      https://docs.oracle.com/javase/6/docs/
 * Wette
 */

//package sportwetten

import java.util.*;
import java.io.*;
import java.time.*;

class Wette implements Serializable,Comparable
{
	private static int wetten_anzahl=0;
	
	private final int wett_id;
	
	private final Sportereignis sportereignis;
	
	private final Spieler spieler;

	private TreeSet<Tipp> tipps=new TreeSet<Tipp>();

	private final float quote_sieg_heim;

	private final float quote_unentschieden;

	private final float quote_niederlage_heim;
	
	private String wett_status;
	
	private String wett_datum;
	
	private String wett_uhrzeit;
	
	//--------------------
	
	public Wette(Sportereignis sportereignis,Spieler spieler,float quote_sieg_heim,float quote_unentschieden,float quote_niederlage_heim)
	{
		this.wett_id=++wetten_anzahl;

		this.sportereignis=sportereignis;
		
		this.spieler=spieler;

		this.quote_sieg_heim=getWett_bar(quote_sieg_heim);
		
		this.quote_unentschieden=getWett_bar(quote_unentschieden);
		
		this.quote_niederlage_heim=getWett_bar(quote_niederlage_heim);

		this.wett_status="offen";

		this.wett_datum=new Date().toString();
		
		this.wett_uhrzeit=wett_datum;
	}
	
	//---get--------
	
	public int getWett_id()
	{
		return wett_id;
	}
	
	public Sportereignis getSportereignis()
	{
		return sportereignis;
	}
	
	public String getAlle_tipps()
	{
		String rueckgabe="";

		Iterator<Tipp>itr=tipps.iterator();

		while(itr.hasNext())
		{
			rueckgabe+="\n"+itr.next().dossier();
		}

		return rueckgabe;
	}

	public String getTipps_von_spieler(Spieler spieler)
	{
		String rueckgabe="";

		Iterator<Tipp>itr=tipps.iterator();

		while(itr.hasNext())
		{
			Tipp temp=itr.next();
			if(temp.getSpieler().getSpieler_id()==spieler.getSpieler_id())
			rueckgabe+="\n"+temp.dossier();
		}

		return rueckgabe;
	}

	public float getQuote_sieg_heim()
	{
		return quote_sieg_heim;
	}
	
	public float getQuote_unentschieden()
	{
		if(sportereignis.getHat_unentschieden())
		{
			return quote_unentschieden;
		}
		else
		{
			return 0;
		}
	}
	
	public float getQuote_niederlage_heim()
	{
		return quote_niederlage_heim;
	}
	
	public float getQuote_endergebins()
	{
		return 3.0F;
	}

	public String getWett_status()
	{
		return wett_status;
	}

	public String getWett_datum()
	{
		return wett_datum;
	}

	public String getWett_uhrzeit()
	{
		return wett_uhrzeit;
	}

	public int getAnzahl_tipps()
	{
		return tipps.size();
	}
	
	public int getAnzahl_tipps(String status)
	{
		int tipp_zaehler=0;
		
		Iterator<Tipp>itr=tipps.iterator();
		
		while(itr.hasNext())
		{
			
			Tipp tipp_temp=(Tipp)itr.next();
			//if()
			//{
				
			//}
		}
		return tipp_zaehler;
	}
	
	public int getWetteinsaetze()
	{
		int einsaetze=0;
		for(Tipp money:tipps)
		{
			einsaetze+=money.getBetrag_euro_tipp();
		}
		
		return einsaetze;
	}

	public String dossier()
	{
		//Wie viele Tipps Geld Ablauf ... 
		return "Wette "+getWett_id()+" "+sportereignis.dossier()+" mit "+getAnzahl_tipps()+" Tipps mit insgesammt "+getWetteinsaetze()+" Euro Eins�tzen,"+
		
		"und endet am "+getWett_datum()+" um "+getWett_uhrzeit()+" und ist "+getWett_status()+".";
	}
	
	
	//---set-------
	
	public boolean setWett_status(Spieler ang_spieler,String w_status)
	{
		//offen storniert geschlossen
		//wett_datum; wett_uhrzeit;
		if((wett_status=="offen")&&(this.spieler!=ang_spieler) )
		{
			wett_status=w_status;
			return true;
		}
		else
		{
			return false;
		}
	}

	/*
	public void setQuote_unentschieden(float q_unentschieden)
	{
		if((!quote_abgeschlossen))//und akt.Datum Und Zeit kleiner Start Datum und 30 min
		{
			if(sportereignis.getHat_unentschieden())
			{
				this.quote_unentschieden=q_unentschieden;
			}
			else
			{
				this.quote_unentschieden=0;
			}
		}
	}
	
	
	public void setQuote_sieg_heim(float q_sieg_heim)
	{
		if((!quote_abgeschlossen))//und akt.Datum Und Zeit kleiner Start Datum und 30 min
		{
			this.quote_sieg_heim=q_sieg_heim;
		}
	}
	
	
	
	///////////////////////
	*/

	//----Methoden--------------
	public boolean tipp_an_wette_abgeben(Tipp tipp)
	{
		Iterator<Tipp>itr=tipps.iterator();
		
		while(itr.hasNext())
		{
			Spieler temp=itr.next().getSpieler();

			if(tipp.getSpieler().getSpieler_id()==temp.getSpieler_id())
			{
				return false;
			}
		}
		return addTipp(tipp);
	}

	public String tipps_auswerten()//void
	{
		String rueckgabe="";

		Iterator<Tipp>itr=tipps.iterator();
		
		while(itr.hasNext())
		{
			Tipp temp=itr.next();

			//String getTipp()
			if(temp.getTipp().equals(sportereignis.getEndergebnis()))
			{
				rueckgabe+="Gewinn "+temp.getSpieler().getBezeichnung()+" hat auf "+temp.getTipp()+" getippt mit "+temp.getBetrag_euro_tipp()+" Euro Einsatz mit "+

				(getQuote_unentschieden()*temp.getBetrag_euro_tipp())+" Euro Gewinn."; //getQuote_endergebins()
				
				temp.getSpieler().einzahlen_Guthaben_euro((float)(getQuote_unentschieden()*temp.getBetrag_euro_tipp()));
			}
		}
		return rueckgabe;
	}
	
	public float getWett_bar(float quote)
	{
		if(wett_datum==wett_datum)
		{
			return quote;
		}
		else
		{
			return 0;
		}
	}


	//------adder-------------------
	
	public boolean addTipp(Tipp tipp)
	{
		if(null!=tipp)
		{
			return tipps.add(tipp);
		}
		else
		{
			return false;
		}
	}
	
	
	//-----Override-------------------------
	
	@Override
	public int hashCode()
	{
		return 11+sportereignis.hashCode();
	}

	@Override
	public boolean equals(Object obj)
	{
		if(this==obj)
		{
			return true;
		}
		else
		{
			if(null!=obj)
			{
				if(obj instanceof Wette)
				{
					Wette temp_wette=(Wette)obj;

					if(this.getSportereignis().equals(temp_wette.getSportereignis()))
					{
						return true;
					}
				}
				return false;
			}
			return false;
		}
	}
	
	
	@Override
	public int compareTo(Object obj)
	{
		return 1;
	}
	
	
	
	
	
	
}
