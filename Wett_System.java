/*
 * R.Schneider
 * Projekt Sportwetten
 * 29.03.2018
 * //git clone https://christopherpike@bitbucket.org/christopherpike/*.git
 * // git commit -m "Initial commit" git push -u origin master      https://docs.oracle.com/javase/6/docs/
 * Wett_System
 */

//package sportwetten

import java.util.*;
import java.io.*;

class Wett_System implements Serializable
{
	private TreeSet<Spieler>spieler;
	
	private TreeSet<Sportereignis>sportereignisse;
	
	//private Dateioperationen dateiop;

	//--------------------
	
	public Wett_System()
	{
		spieler=new TreeSet<Spieler>();
		
		sportereignisse=new TreeSet<Sportereignis>();
	}
	
	//-----------
	
	public String dossier()
	{
		return "Wett_System";
	}
	
	
	
	//----get------
	
	public Spieler getSpieler(int id)
	{
		return null;
	}
	
	public Sportereignis getSportereignis(int sportereignis_id)
	{
		return null;
	}
	
	public int getAnzahl_Spieler()
	{
		return spieler.size();
	}
	
	public int getAnzahl_Sportereignisse()
	{
		return sportereignisse.size();
	}
	
	public int getAnzahl_Wetten()
	{
		int wetten_zaehler=0;
		
		Iterator<Sportereignis>itr=sportereignisse.iterator();
		
		while(itr.hasNext())
		{
			Sportereignis sp_temp=(Sportereignis)itr.next();
			
			if(null!=sp_temp.getWette())//getWette()
			{
				++wetten_zaehler;
			}
		}
		return wetten_zaehler;
	}
	
	public int getAnzahl_Wetten_mit_status(String status)
	{
		int wetten_status_zaehler=0;

		Iterator<Sportereignis>itr=sportereignisse.iterator();

		while(itr.hasNext())
		{
			Sportereignis sp_temp=(Sportereignis)itr.next();

			if(null!=sp_temp.getWette())//getWette()
			{
				if(sp_temp.getWette().getWett_status().equals(status))
				{
					++wetten_status_zaehler;
				}
			}
		}
		return wetten_status_zaehler;
	}
	
	public int getAnzahl_Tipps()
	{
		int tipps_zaehler=0;

		Iterator<Sportereignis>itr=sportereignisse.iterator();

		while(itr.hasNext())
		{
			Sportereignis sp_temp=(Sportereignis)itr.next();

			if(null!=sp_temp.getWette())//getWette()
			{
				tipps_zaehler+=sp_temp.getWette().getAnzahl_tipps();
			}
		}
		return tipps_zaehler;
	}
	
	public int getAnzahl_Tipps(String status)
	{
		if(null==status)
		{
			status="offen";
		}

		int tipps_status_zaehler=0;

		Iterator<Sportereignis>itr=sportereignisse.iterator();

		while(itr.hasNext())
		{
			Sportereignis sp_temp=(Sportereignis)itr.next();

			if(null!=sp_temp.getWette())
			{
				switch(status)
				{
					case"offen":
						{
							tipps_status_zaehler+=sp_temp.getWette().getAnzahl_tipps("offen");
							break;
						}
					case"geschlossen":
						{
							tipps_status_zaehler+=sp_temp.getWette().getAnzahl_tipps("geschlossen");
							break;
						}
					case"x":
						{
							tipps_status_zaehler+=sp_temp.getWette().getAnzahl_tipps("offen");
							break;
						}
					default:
						{
							//
						}
				}


				if(sp_temp.getWette().getWett_status().equals(status))
				{
					//tipps_zaehler+=sp_temp.getWette().getAnzahl_tipps();
					//getAnzahl_tipps(String status)
					++tipps_status_zaehler;
				}
			}
		}
		return tipps_status_zaehler;
	}

	//-----set------
	
	//------adder---------------
	
	public boolean addSpieler(Spieler spieler)
	{
		if(null!=spieler)
		{
			return this.spieler.add(spieler);
		}
		return false;
	}
	
	public boolean addSportereignis(Sportereignis sportereignis)
	{
		if(null!=sportereignis)
		{
			return sportereignisse.add(sportereignis);
		}
		return false;
	}
}
