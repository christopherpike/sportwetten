

import java.util.*;
import java.io.*;

public class Sandbox
{
	private TreeSet<User>users;
	
	public Sandbox()
	{
		this.users=new TreeSet<User>();
	}
	
	public boolean addUser(User user)
	{
		return users.add(user);
	}
	
	public int getAnzahlUsers()
	{
		return users.size();
	}
	
	public static void main(String[]arguments)
	{
		Sandbox sb=new Sandbox();
		
		User u1=new User("Rene","Schneider");
		
		User u2=new User("RENE","Schneider");

		User u3=new User("Christopher","Schneider");

		System.out.println(u1.getInfo());
		
		System.out.println(u1);

		System.out.println(u1.hashCode());
		
		System.out.println(u2.getInfo());

		System.out.println(u2);
		
		System.out.println(u2.hashCode());
		
		System.out.println(u3.getInfo());


		System.out.println(u1.equals(u2));
		

		sb.addUser(u1);

		sb.addUser(u2);
		
		sb.addUser(u3);


		System.out.println(sb.getAnzahlUsers());

	}
}
