/*
 * R.Schneider
 * Projekt Sportwetten
 * 29.03.2018
 * //git clone https://christopherpike@bitbucket.org/christopherpike/*.git
 * // git commit -m "Initial commit" git push -u origin master      https://docs.oracle.com/javase/6/docs/
 * Model
 */

//package sportwetten

import java.util.*;


class Model
{
	private static int spielerAnzahl=0;
	
	private int id=0;
	
	private Wett_System wett_system;
	
	private Spieler ang_spieler;
	
	//--------------------
	
	public Model()
	{
		this.wett_system=new Wett_System();
	}
	
	//-----------
	
	public String dossier()
	{
		return "Model";
	}
	
	
	
	//----------
	
	private void setAng_spieler(Spieler spieler)
	{
		if(null!=spieler&&null==ang_spieler)
		{
			this.ang_spieler=spieler;
		}
	}
	
	private void removeAng_spieler()
	{
		this.ang_spieler=null;
	}
	
	public boolean spieler_anlegen(String spieler_vor_name,String spieler_nach_name,String passwort,float startguthaben)
	{
		//Werkzeug.
		//if(null!=xxx && n && n &&)
		Spieler kumpel_t=new Spieler("Vadym","Zher......","cdsfsf",10.00F);

		Sportereignis fb_schalke_bayern=new Sportereignis("fussball","Schalke 04","FC Bayern",true,"18.03.2018","19:00");
		Wette wette_fb_sch_bay=new Wette(fb_schalke_bayern,kumpel_t,3.5F,3.0F,1.0F);

		System.out.println(fb_schalke_bayern.setWette(wette_fb_sch_bay));

		System.out.println(wett_system.addSportereignis(fb_schalke_bayern));
		

		Spieler kumpel=new Spieler(spieler_vor_name,spieler_nach_name,passwort,startguthaben);
		
		System.out.println(wett_system.getAnzahl_Spieler());
		System.out.println(wett_system.getAnzahl_Wetten());
		System.out.println(wett_system.getAnzahl_Wetten_mit_status("offen"));
		System.out.println(wett_system.getAnzahl_Wetten_mit_status("geschlossen"));
		System.out.println(wett_system.getAnzahl_Wetten_mit_status("storniert"));
		System.out.println("Tipps "+wett_system.getAnzahl_Tipps("offen"));
		
		
		//Status zeile ........
		return wett_system.addSpieler(kumpel);
	}
	
	public boolean spieler_anmelden(String spieler_vor_name,String spieler_nach_name,String passwort)
	{
		
		//setAng_spieler(Spieler spieler);
		
		
		return false;
	}
	
	public boolean sportereignis_anlegen(String sportart,String mannschaft_heim,String mannschaft_gast,boolean hat_unentschieden,String sp_datum,String sp_uhrzeit)
	{
		//Sportereignis fb_schalke_bayern=new Sportereignis("fussball","Schalke 04","FC Bayern",true,"18.03.2018","19:00");
		return false;
	}
	
	public boolean wette_anlegen(Sportereignis sportereignis,Spieler spieler,float quote_sieg_heim,float quote_unentschieden,float quote_niederlage_heim)
	{
		//Wette wette_fb_sch_bay=new Wette(fb_schalke_bayern,kumpel,3.5F,3.0F,1.0F);
		return false;
	}
	
	public boolean tipp_abgeben(Spieler spieler,String tipp_art,int tipp_betrag_euro)
	{
		//Tipp hypothek=new Tipp(kumpel,"Sieg Heim",100);
		return false;
	}
}
