// User

class User implements Comparable
{
	private static int alleUser;
	
	private int userID;
	
	private String vorname;
	
	private String nachname;
	
	//-------
	
	public User(String vorname,String nachname)
	{
		this.userID=alleUser++;
		
		this.vorname=vorname.trim();
		
		this.vorname=vorname.toLowerCase();
		
		this.nachname=nachname.trim();
		
		this.nachname=nachname.toLowerCase();
	}
	
	//----
	
	public String getVorname()
	{
		return vorname;
	}
	
	public String getNachname()
	{
		return nachname;
	}

	public String getVollerName()
	{
		return vorname+" "+nachname;
	}

	public String getInfo()
	{
		return "ID "+userID+" Vorname "+vorname+" Nachname "+nachname;
	}
	
	//
	
	@Override
	public int hashCode()
	{
		return 11+vorname.hashCode()+nachname.hashCode();
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if(this==obj)
		{
			return true;
		}
		else
		{
			if(null!=obj)
			{
				if(obj instanceof User)
				{
					User temp_user=(User)obj;

					if(this.vorname.equals(temp_user.getVorname()))
					{
						if(this.nachname.equals(temp_user.getNachname()))
						{
							return true;
						}
						return false;
					}
					return false;
				}
				return false;
			}
			return false;
		}
	}
	
	@Override
	public int compareTo(Object obj)
	{
		return this.getVollerName().compareTo(((User)obj).getVollerName());
	}
}
