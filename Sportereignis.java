/*
 * R.Schneider
 * Projekt Sportwetten
 * 29.03.2018
 * //git clone https://christopherpike@bitbucket.org/christopherpike/*.git
 * // git commit -m "Initial commit" git push -u origin master      https://docs.oracle.com/javase/6/docs/
 * Sportereignis
 */

//package sportwetten

import java.util.*;
import java.io.*;

class Sportereignis implements Serializable,Comparable
{
	private static int sportereignis_anzahl=0;
	
	private final int sportereignis_id;
	
	private final String sportart;

	private final String mannschaft_heim;

	private final String mannschaft_gast;
	
	private boolean hat_unentschieden=false;
	
	private final String ereignisstart_datum;
	
	private final String ereignisstart_uhrzeit;
	
	private Wette wette;

	//-----

	private boolean quote_abgeschlossen=false;

 	private int ergebnis_heim=0;
	
	private int ergebnis_gast=0;
	
	//--------------------
	
	public Sportereignis(String sportart,String mannschaft_heim,String mannschaft_gast,

							boolean hat_unentschieden,String ereignisstart_datum,String ereignisstart_uhrzeit)
	{
		this.sportereignis_id=++sportereignis_anzahl;

		this.sportart=sportart;
		
		this.mannschaft_heim=mannschaft_heim.trim();
		
		this.mannschaft_gast=mannschaft_gast.trim();
		
		this.hat_unentschieden=hat_unentschieden;
		
		this.ereignisstart_datum=ereignisstart_datum.trim();

		this.ereignisstart_uhrzeit=ereignisstart_uhrzeit.trim();
	}
	
	//---get--------

	public int getSportereignis_anzahl()
	{
		return sportereignis_anzahl;
	}

	public int getSportereignis_id()
	{
		return sportereignis_id;
	}

	public String getSportart()
	{
		return sportart;
	}

	public String getMannschaft_heim()
	{
		return mannschaft_heim;
	}

	public String getMannschaft_gast()
	{
		return mannschaft_gast;
	}
	
	public boolean getHat_unentschieden()
	{
		return hat_unentschieden;
	}

	public String getEreignisstart_datum()
	{
		return ereignisstart_datum;
	}

 	public String getEreignisstart_uhrzeit()
	{
		return ereignisstart_uhrzeit;
	}
	
	public Wette getWette()
	{
		//clone
		return wette;
	}

	//----Quoten-------------
	
	public boolean isQuote_abgeschlossen()
	{
		return quote_abgeschlossen;
	}

	public boolean isUnentschieden()
	{
		if(ergebnis_heim==ergebnis_gast)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public int getErgebnis_heim()
	{
		if(quote_abgeschlossen)
		{
			return ergebnis_heim;
		}
		return 0;
	}

	public int getErgebnis_gast()
	{
		if(quote_abgeschlossen)
		{
			return ergebnis_gast;
		}
		return 0;
	}
	
	public String getEndergebnis()
	{
		if(quote_abgeschlossen)
		{
			if(getErgebnis_heim()>getErgebnis_gast())
			{
				return "Sieg Heim";
			}
			else if(getErgebnis_heim()<getErgebnis_gast())
			{
				return "Niederlage Heim";
			}
			else
			{
				return "Unentschieden";
			}
		}
		return "Spiel l�uft noch";
	}

	public String dossier()
	{
		return "Sportereignis Nummer "+sportereignis_id+" "+sportart+": "+mannschaft_heim+" gegen "+mannschaft_gast+", am "+ereignisstart_datum+" um "+ereignisstart_uhrzeit+" Uhr.";
	}
	
	public String dossier_intern()
	{
		return "Sportereignis Nummer "+getSportereignis_id()+" "+getSportart()+": "+getMannschaft_heim()+" gegen "+getMannschaft_gast()+", am "+getEreignisstart_datum()+" um "+getEreignisstart_uhrzeit()+" Uhr.";
	}
	
	//----set------
	
	public boolean setWette(Wette wette)
	{
		if(null!=wette)//datumscheck &&(0==ereignisstart_datum.equals(ereignisstart_datum)))
		{
			if(null==this.wette)
			{
				this.wette=wette;
				return true;
			}
			else
			{
				return false;
			}
		}
		return false;
	}
	
	//-----Quoten
	
	//----Quoten-------------

	public void setQuote_abgeschlossen(boolean q_abgeschlossen)
	{
		this.quote_abgeschlossen=q_abgeschlossen;
	}

	public void setErgebnis_heim(int e_heim)
	{
		if((quote_abgeschlossen))//und akt.Datum Und Zeit gr��er Start Datum und 30 min
		{
			this.ergebnis_heim=e_heim;
		}
		
	}

	public void setErgebnis_gast(int e_gast)
	{
		if((quote_abgeschlossen))//und akt.Datum Und Zeit gr��er Start Datum und 30 min
		{
			this.ergebnis_gast=e_gast;
		}
		
	}
	
	
	
	//----overrides----------
	
	@Override
	public int hashCode()
	{
		int hc=0; int mm=11;
		
		hc+=mm+mannschaft_heim.hashCode()+mannschaft_gast.hashCode()+ereignisstart_datum.hashCode()+ereignisstart_uhrzeit.hashCode();
		
		return hc;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if(this==obj)
		{
			return true;
		}
		else
		{
			if(null!=obj)
			{
				if(obj instanceof Sportereignis)
				{
					Sportereignis temp_spe=(Sportereignis)obj;

					if(this.getSportart().equals(temp_spe.getSportart()))
					{
						if(this.getMannschaft_heim().equals(temp_spe.getMannschaft_heim()))
						{
							if(this.getMannschaft_gast().equals(temp_spe.getMannschaft_gast()))
							{
								if(this.getEreignisstart_datum().equals(temp_spe.getEreignisstart_datum()))
								{
									if(this.getEreignisstart_uhrzeit().equals(temp_spe.getEreignisstart_uhrzeit()))
									{
										return true;
									}
									return false;
								}
								return false;
							}
							return false;
						}
						return false;
					}
					return false;
				}
				return false;
			}
			return false;
		}
	}

	@Override
	public int compareTo(Object obj)
	{
		
		return 1;
	}
	
	
	
}
