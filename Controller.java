/*
 * R.Schneider
 * Projekt Sportwetten
 * 29.03.2018
 * //git clone https://christopherpike@bitbucket.org/christopherpike/*.git
 * // git commit -m "Initial commit" git push -u origin master      https://docs.oracle.com/javase/6/docs/
 * Controller
 */

//package sportwetten

import java.util.*;
import java.awt.event.*;

class Controller
{
	private Model model;
	
	private View view;

	//--------------------
	
	public Controller()
	{
		this.view=new View(new CActionListener());
		this.model=new Model();
	}
	
	//-----------
	
	public String dossier()
	{
		return "Controller";
	}
	
	class CActionListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent ae)
		{
			String str=ae.getActionCommand();
			
			switch(str)
			{
				case "Hautpmenue":
					{
						//view.remove(pnl_spieler_anlegen);
						//view.add(pnl_spieler_anmelden);
						break;
					}
				case "Spieler anlegen":
					{

						System.out.println(model.spieler_anlegen("Hen","Ben","kjff�k�l",10.00F));
						break;
					}
				case "Spieler anmelden":
					{
						//spieler_anmelden(String spieler_vor_name,String spieler_nach_name,String passwort);

						System.out.println(model.spieler_anlegen("Hen","Ben","kjff�k�l",10.00F));
						break;
						
					}
				case "Sportereignis anlegen":
					{


						System.out.println(model.spieler_anlegen("Hen","Ben","kjff�k�l",10.00F));
						break;
					}
				case "Wette anlegen":
					{


						System.out.println(model.spieler_anlegen("Hen","Ben","kjff�k�l",10.00F));
						break;
					}
				case "Tipp abgeben":
					{


						System.out.println(model.spieler_anlegen("Hen","Ben","kjff�k�l",10.00F));
						break;
					}
				default:
					{
					System.out.println("Watt?");
					}
			}
		}
	}
	
	//-------get----
	
	//-----set----
	
	
	public static void main(String[]arguments)
	{
		Controller con=new Controller();
		
		Spieler kumpel_head=new Spieler("Hen","Ben","kjff�k�l",10.00F);

		Spieler kumpel=new Spieler("Vadym","Zher......","cdsfsf",10.00F);
		
		System.out.println(kumpel);
		
		System.out.println(kumpel.dossier());
		

		//Sportereignis(String typ,String mannschaft_heim,String mannschaft_gast,boolean hat_unentschieden,String ereignisstart_datum,String ereignisstart_uhrzeit);
		Sportereignis fb_schalke_bayern=new Sportereignis("fussball","Schalke 04","FC Bayern",true,"18.03.2018","19:00");
		
		System.out.println(fb_schalke_bayern);
		
		System.out.println(fb_schalke_bayern.dossier_intern());
		
		System.out.println(fb_schalke_bayern.dossier_intern());
		
		//Wette(Sportereignis sportereignis,float quote_sieg_heim,float quote_unentschieden,float quote_niederlage_heim)
		Wette wette_fb_sch_bay=new Wette(fb_schalke_bayern,kumpel,3.5F,3.0F,1.0F);
		
		Tipp hypothek=new Tipp(kumpel,"Sieg Heim",100);
		
		Tipp scheidung=new Tipp(kumpel_head,"Niederlage Heim",200);
		
		Tipp kein_geld_mehr=new Tipp(kumpel,"Unentschieden",200);

		Tipp kein_geld=new Tipp(kumpel_head,"Unentschieden",400);


		System.out.println("Vergleich "+kein_geld.equals(scheidung));



		System.out.println(wette_fb_sch_bay.tipp_an_wette_abgeben(hypothek));
		
		System.out.println(wette_fb_sch_bay.tipp_an_wette_abgeben(scheidung));
		
		System.out.println(wette_fb_sch_bay.tipp_an_wette_abgeben(kein_geld_mehr));

		System.out.println(wette_fb_sch_bay.tipp_an_wette_abgeben(kein_geld));


		System.out.println(wette_fb_sch_bay.dossier());
		
		System.out.println(new Date());
		
		System.out.println(wette_fb_sch_bay.getAlle_tipps());
		
		System.out.println(wette_fb_sch_bay.getTipps_von_spieler(kumpel_head));
		
		fb_schalke_bayern.setQuote_abgeschlossen(true);

		fb_schalke_bayern.setErgebnis_heim(7);

		fb_schalke_bayern.setErgebnis_gast(10);

		
		System.out.println(fb_schalke_bayern.getEndergebnis());


		System.out.println(kumpel.getGuthaben_euro());

		System.out.println(wette_fb_sch_bay.tipps_auswerten());
		
		System.out.println(kumpel.getGuthaben_euro());
		
	}
}
