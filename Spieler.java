/*
 * R.Schneider
 * Projekt Sportwetten
 * 29.03.2018
 * //git clone https://christopherpike@bitbucket.org/christopherpike/*.git
 * // git commit -m "Initial commit" git push -u origin master      https://docs.oracle.com/javase/6/docs/
 * Spieler
 */

//package sportwetten

//import java.util.*;
import java.io.*;

class Spieler implements Serializable,Comparable
{
	private static int spieler_anzahl=0;
	
	private final int spieler_id;
	
	private String vor_name;
	
	private String nach_name;
	
	private String passwort;
	
	private float guthaben_euro=0;
	
	//--------------------
	
	public Spieler(String vor_name,String nach_name,String passwort,float guthaben_euro)
	{
		this.spieler_id=++spieler_anzahl;

		this.vor_name=vor_name.trim();
		
		this.vor_name=Werkzeug.erster_buchstabe_gross(vor_name.toLowerCase());

		this.nach_name=nach_name.trim();
		
		this.nach_name=Werkzeug.erster_buchstabe_gross(nach_name.toLowerCase());

		this.passwort=passwort.trim();
		
		this.guthaben_euro=guthaben_euro;
	}
	
	//----get----------------------
	
	public int getSpieler_id()
	{
		return spieler_id;
	}

	public String getVor_name()
	{
		return vor_name;
	}

	public String getNach_name()
	{
		return nach_name;
	}
	
	public String getVollerName()
	{
		return vor_name+" "+nach_name;
	}

	public String getBezeichnung()
	{
		return "Spieler "+spieler_id+" "+vor_name+" "+nach_name;
	}


	public float getGuthaben_euro()
	{
		return guthaben_euro;
	}

	public String dossier()
	{
		return "Spieler "+spieler_id+" "+vor_name+" "+nach_name+" hat "+guthaben_euro+" Euro Guthaben.";
	}
	
	public String dossier_intern()
	{
		return "Spieler "+spieler_id+" "+vor_name+" "+nach_name+" hat "+guthaben_euro+" Euro Guthaben.";
	}

	//------set------------------

	public float einzahlen_Guthaben_euro(float betrag)
	{
		return guthaben_euro+=betrag;
	}
	
	public float auszahlen_Guthaben_euro(float auszahlungs_betrag)
	{
		if(guthaben_euro<=auszahlungs_betrag)
		{
			//Betrag wird abgezogen
			guthaben_euro-=auszahlungs_betrag;
			return auszahlungs_betrag;
		}
		else
		{
			return 0;
		}
	}
	
	public int auszahlen_Guthaben_euro(int auszahlungs_betrag)
	{
		if(guthaben_euro<=auszahlungs_betrag)
		{
			//Betrag wird abgezogen
			guthaben_euro-=auszahlungs_betrag;
			return auszahlungs_betrag;
		}
		else
		{
			return 0;
		}
	}
	
	
	public void setGuthaben_euro(float betrag)
	{
		this.guthaben_euro=betrag;
	}
	
	
	
	//----overrides------
	
	@Override
	public int hashCode()
	{
		int hc=0; int mm=11;
	
		hc+=mm+getVollerName().hashCode();

		return hc;
	}
	
	//equals
	@Override
	public boolean equals(Object obj)
	{
		if(this==obj)
		{
			return true;
		}
		if(null!=obj)
		{
			if(obj instanceof Spieler)
			{
				Spieler temp=(Spieler)obj;

				if(temp.getVor_name().equals(getVor_name()))
				{
					if(temp.getNach_name().equals(getNach_name()))
					{
							return true;
					}
					return false;
				}
				return false;
			}
			return false;
		}
		return false;
	}
	
	@Override
	public int compareTo(Object obj)
	{
		Spieler temp_spieler=null;
		
		if(obj instanceof Spieler)
		{
			temp_spieler=(Spieler)obj;
		}
		
		return this.getVollerName().compareTo(temp_spieler.getVollerName());
	}
}
